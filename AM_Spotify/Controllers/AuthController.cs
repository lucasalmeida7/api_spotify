﻿using AM_Spotify.Config;
using AM_Spotify.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AM_Spotify.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Index()
        {
            string code = HttpContext.Request.QueryString["code"];
            GerarToken(code);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Login()
        {
            return Redirect(@"https://accounts.spotify.com/authorize/?" + 
                "client_id=" + Spotify_Config.ClientID + "&response_type=code" +
                "&redirect_uri=" + Spotify_Config.RedirectUri + 
                "&scope=user-read-private%20user-read-email%20playlist-modify-private%20playlist-modify-public");
        }

        private void GerarToken(string code)
        {
            SpotifyService spotifyService = new SpotifyService();
            spotifyService.GenerateToken(code);
        }
        
    }
}