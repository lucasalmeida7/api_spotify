﻿using AM_Spotify.Models;
using AM_Spotify.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AM_Spotify.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(ApiManager.GetKey(ApiManager.Spotify)))
            {
                return RedirectToAction("Index", "Login");
            }

            return View();
        }

        public ActionResult About()
        {
            if (string.IsNullOrEmpty(ApiManager.GetKey(ApiManager.Spotify)))
            {
                return RedirectToAction("Index", "Login");
            }

            ViewBag.Message = "Objetivos do trabalho.";

            return View();
        }

        public ActionResult Contact()
        {
            if (string.IsNullOrEmpty(ApiManager.GetKey(ApiManager.Spotify)))
            {
                return RedirectToAction("Index", "Login");
            }

            ViewBag.Message = "Email: magmapt@gmail.com";

            return View();
        }
    }
}