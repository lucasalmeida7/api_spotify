﻿using AM_Spotify.Models;
using AM_Spotify.Services;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AM_Spotify.Controllers
{
    public class PlayListController : Controller
    {
        // GET: PlayList
        public ActionResult Index()
        {
            string token = ApiManager.GetKey(ApiManager.Spotify);
            bool hasToken = !string.IsNullOrEmpty(token);

            if (!hasToken)
            {
                return RedirectToAction("Index", "Login");
            }

            List<PlayList> playlists = null;
            
            SpotifyService spotifyService = new SpotifyService();
            playlists = spotifyService.GetPlaylists(token);
            
            ViewBag.PlayLists = playlists;

            return View();
        }

        public ActionResult Tracks(string idPlayList)
        {
            string token = ApiManager.GetKey(ApiManager.Spotify);
            bool hasToken = !string.IsNullOrEmpty(token);

            if (!hasToken)
            {
                return RedirectToAction("Index", "Login");
            }

            List<Track> tracks = null;
            if (hasToken)
            {
                SpotifyService spotifyService = new SpotifyService();
                tracks = spotifyService.GetPlayListTracks(token, idPlayList);
            }

            ViewBag.Tracks = tracks;
            return View();
        }

    }
}