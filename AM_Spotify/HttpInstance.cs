﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AM_Spotify
{
    public class HttpInstance
    {
        private static HttpClient httpClienteInstance;

        public static HttpClient GetHttpClientInstance()
        {
            if (httpClienteInstance == null)
            {
                httpClienteInstance = new HttpClient();
                httpClienteInstance.DefaultRequestHeaders.ConnectionClose = false;
            }

            return httpClienteInstance;
        }

    }
}