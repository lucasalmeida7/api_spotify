﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AM_Spotify.Models
{
    public class PlayList
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Criador { get; set; }
        public string Link { get; set; }
    }
}