﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AM_Spotify.Models
{
    public class Track
    {
        public string Nome { get; set; }
        public string Album { get; set; }
        public string Artista { get; set; }
    }

}