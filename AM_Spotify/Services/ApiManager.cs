﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AM_Spotify.Services
{
    public class ApiManager
    {
        public static string Spotify = "spotify";

        private static Dictionary<string, string> keys = new Dictionary<string, string>();

        public static void AddKey(string name, string value)
        {
            keys.Add(name, value);
        }

        public static string GetKey(string name)
        {
            if (!keys.ContainsKey(name))
                return string.Empty;

            return keys[name];
        }

    }
}