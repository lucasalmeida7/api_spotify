﻿using AM_Spotify.Config;
using AM_Spotify.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AM_Spotify.Services
{
    public class SpotifyService
    {
        private const string accountUrl = "https://accounts.spotify.com";

        public string BaseUrl
        {
            get
            {
                return "https://api.spotify.com";
            }
        }

        public void GenerateToken(string code)
        {
            string action = "/api/token";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, accountUrl + action);

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("grant_type", "authorization_code"));
            parameters.Add(new KeyValuePair<string, string>("code", code));
            parameters.Add(new KeyValuePair<string, string>("redirect_uri", Spotify_Config.RedirectUri));
            parameters.Add(new KeyValuePair<string, string>("client_id", Spotify_Config.ClientID));
            parameters.Add(new KeyValuePair<string, string>("client_secret", Spotify_Config.Client_Secret));

            request.Content = new FormUrlEncodedContent(parameters);

            HttpResponseMessage response = HttpInstance.GetHttpClientInstance().SendAsync(request).Result;

            JObject tokenJson = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            string token = tokenJson["access_token"].ToString();

            ApiManager.AddKey(ApiManager.Spotify, token);
        }

        public List<PlayList> GetPlaylists(string token)
        {
            string action = "/v1/me/playlists";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, BaseUrl + action);

            request.Headers.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = HttpInstance.GetHttpClientInstance().SendAsync(request).Result;

            JArray playlistsJson = (JArray)JObject.Parse(response.Content.ReadAsStringAsync().Result)["items"];

            List<PlayList> playlists = new List<PlayList>();

            foreach (var playlistJson in playlistsJson)
            {
                playlists.Add(new PlayList() {
                    Id = playlistJson["id"].ToString(),
                    Nome = playlistJson["name"].ToString(),
                    Criador = playlistJson["owner"]["display_name"].ToString(),
                    Link = playlistJson["external_urls"]["spotify"].ToString()
                });
            }

            return playlists;
        }

        public List<Track> GetPlayListTracks(string token, string idPlayList)
        {
            string action = "/v1/playlists/" + idPlayList + "/tracks";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, BaseUrl + action);

            request.Headers.Add("Authorization", "Bearer " + token);

            HttpResponseMessage response = HttpInstance.GetHttpClientInstance().SendAsync(request).Result;

            JArray tracksJson = (JArray)JObject.Parse(response.Content.ReadAsStringAsync().Result)["items"];

            List<Track> tracks = new List<Track>();

            foreach (var trackJson in tracksJson)
            {
                tracks.Add(new Track()
                {
                    Nome = trackJson["track"]["name"].ToString(),
                    Album = trackJson["track"]["album"]["name"].ToString(),
                    Artista = trackJson["track"]["artists"][0]["name"].ToString()
                });
            }
            
            return tracks;
        }

    }

}